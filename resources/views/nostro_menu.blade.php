@extends('layouts.front')

@section('content')

<div>
    <div class="container">

        <div class="row font_vadodara_16px">

            <div class="col-sm-2 col_categories">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <h5 class="title_col_menu">Categorie</h5>
                    </li>
                    @if( $menu_categories!=null )
                        @for($i = 0; $i<(count($menu_categories)); $i++)
                            <li class="list-group-item list-group-flush border-0">
                                <a class="a_col_categories" href="#li_{{$i}}"><h6>{{ $menu_categories[$i] }}</h6></a>
                            </li>
                        @endfor
                    @else
                        <li class="list-group-item list-group-flush">
                            <a href="#">Menu vuoto</a>
                        </li>
                    @endif
                </ul>
                <br>
                <br>
            </div>
            <div class="overflow-scroll col-sm-7 col_menu">
                <div class="row col-12">
                    <div class="col-11">
                        <input class="form-control form-control-sm " type="text" placeholder="Cerca nel menu" aria-label="Cerca nel menu">
                    </div>
                    <div class="col-1">
                        <i class="fas fa-search" aria-hidden="true"></i>
                    </div>
                </div>
                <ul class="list-group smooth-scroll list-unstyled  list-group-flush">
                    @if( $menu_categories!=null )
                        @for($i = 0; $i<(count($menu_categories)); $i++)
                        <li id="li_{{$i}}" class="list-group-item list-group-flush"><button class="btn btn-link col_menu" data-toggle="collapse" data-target="#div_{{$i}}" aria-expanded="true" aria-controls="div_{{$i}}"><h5 class="title_col_menu">{{ $menu_categories[$i] }}</h5></button></li>
                            @for($j=0;$j<(count($category_products[$menu_categories[$i]]));$j++)
                                <li id="div_{{$i}}" class="list-group-item collapse show list-group-flush">
                                    <div class="row">
                                        @if($category_products[$menu_categories[$i]][$j]->img!=null)
                                        <div class="col-4"><img src="{{asset("images/".$category_products[$menu_categories[$i]][$j]->img)}}" class="img-fluid" alt="Responsive image"></div>
                                        @else
                                        <div class="col-4"><img src="{{asset("img/logo.png")}}" class="img-fluid" alt="Responsive image"></div>
                                        @endif
                                        <div class="row col-8">
                                            <div class="col-12">
                                                <h6 class="title_col_menu">{{ $category_products[$menu_categories[$i]][$j]->title }}</h6>
                                            </div>
                                            <div class="col-7">{{$category_products[$menu_categories[$i]][$j]->sutitle}}</div>
                                            <div class="col-3">{{$category_products[$menu_categories[$i]][$j]->price}} € </div>
                                            {{-- <div class="col-2"><button type="button" onclick="add_to_cart('{{$category_products[$menu_categories[$i]][$j]->id}}','{{ $category_products[$menu_categories[$i]][$j]->title }}','{{ $category_products[$menu_categories[$i]][$j]->price }}')" class="btn btn-primary b_add_product">+</button></div> --}}
                                            <div class="col-2"><a class="btn btn-outline-primary" href="">Aggiungi al carrello</a></div>
                                        </div>
                                    </div>
                                </li>
                            @endfor
                        @endfor
                    @else
                        <li class="list-group-item">
                                <a href="#">Menu vuoto</a>
                        </li>
                    @endif
                </ul>
                <br>
                <br>
            </div>

            <div class="col-sm-3 col_categories ">
                <br>
                <div><button type="button" class="btn btn-primary col-12">Ordina adesso</button></div>
                <input id="sub_totale" type="hidden" name="sub_totale">
                <input id="totale" type="hidden" name="totale" value="0">

                <ul id="ul_carrello" class="list-group list-group-flush ">
                    <li class="list-group-item list-group-flush">
                        <h5 class="title_col_menu">Carrello</h5>
                    </li>

                </ul>


                <ul id="ciao">

                </ul>

                <ul class="list-group list-group-flush ">
                    <li class="list-group-item list-group-flush border-top">
                        <h6 class="col_carrello"><span class="title_col_menu">Totale</span>  <span class="title_col_menu">€</span></h6>
                    </li>
                </ul>
                <br>
                <br>
            </div>
        </div>

        <div>
            <br>
        </div>
        <div class="modal fade" id="Modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog  modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
                        <button type="button" class="btn btn-primary" onclick="$(this).hide().delay(1000).show(0); $('#click-me').click()">Conferma</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



@endsection

<script>


    function add_to_cart(id,name,prezzo){

        var totale = 0;
        totale +=  parseFloat($('#totale').val());
        $('#totale').val(totale+parseFloat(prezzo));
        var riga = ' <li id="li_cart_'+id+'" class="list-group-item list-group-flush border-0 li_riga_carrello"> '+
                   '     <input type="hidden" id="id_prod_'+id+'" value="'+id+'">'+
                   '     <input type="hidden" id="totale_li_'+id+'" value="0">'+
                   '     <input type="hidden" id="nome_prod_'+id+'" value="'+name+'">'+
                   '     <div class="row">'+
                   '        <div class="col-2 elem_riga_carrello"><button type="button" onclick="remove_from_cart(\''+id+'\')" class="btn btn-primary b_del_product">-</button></div>'+
                   '        <div class="col-2 elem_riga_carrello"> <h6 class="col_carrello">1 x</h6> </div>'+
                   '        <div class="col-5 elem_riga_carrello"> <h6 class="col_carrello">'+name+'</h6> </div>'+
                   '        <div class="col-3 elem_riga_carrello"> <h6 class="col_carrello">'+prezzo+'€</h6> </div>'+
                   '     </div>'+
                   '     <div class="row">'+
                   '        <ul id="ul_variazioni_'+id+'" class="col-4 elem_riga_carrello"></div>'+
                   '        </ul>'+
                   '     </div>'+
                   '     <div class="row">'+
                   '        <div class="col-4 elem_riga_carrello"></div>'+
                   '        <div class="col-4 elem_riga_carrello"></div>'+
                   '        <div class="col-4 elem_riga_carrello"><button data-backdrop="static" data-toggle="modal" data-target="#Modal" type="button"'+
                   '               onclick="$(\'#Modal .modal-body\').load(\'../cart_variations_form/'+id+'\')"'+
                   '               class="btn btn-link elem_riga_carrello">Variazioni</button></div>'+
                   '     </div>'+
                   ' </li>';
        $('#ul_carrello').append(riga);
    }

    function prova(){
        document.getElementById("ciao").innerHTML = riga;
    }


    function remove_from_cart(id){
        $('#li_cart_'+id+'').remove();
    }

</script>
