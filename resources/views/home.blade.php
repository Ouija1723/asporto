@extends('layouts.front')



@section('content')

<div class="parallax-window"></div>

<div class="text-center p-2 border-bottom">

    {{$setting->under_banner}}

</div>

<h1 class="text-center pt-5">{{$setting->h1}}</h1>
<h2 class="text-center pb-5 font-weight-normal">{{$setting->h2}}</h2>


<div>
    <div class="container">
        <div class="card-columns">

            @foreach($news AS $new)

            <div class="card">
                @if($new->img)<img class="card-img-top" src="{{asset('news/'.$new->img)}}" alt="{{$new->title}}">@endif
                <div class="card-body">
                    <h5 class="card-title">{{$new->title}}</h5>
                    <h6><i>{{$new->sutitle}}</i></h6>
                    <div class="card-text text-justify"><small>{!!$new->text!!}</small></div>
                </div>
            </div>

            @endforeach
        </div>



    </div>
</div>



@endsection
