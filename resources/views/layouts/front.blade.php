
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta name="description" content="">
        <meta name="author" content="Dedalus SNC">
        <link rel="icon" href="#">
        <title>{{$setting->name}}</title>

        <link rel="icon" type="image/png" href="{{asset("img/icon.png")}}" />
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;400;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="{{asset("fontawesome/css/all.css")}}" rel="stylesheet">
        <link href="{{asset("css/style.css")}}" rel="stylesheet"> 
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="{{route("home")}}"><img src="{{asset("img/logo.png")}}"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="{{route("home")}}">Home <span class="sr-only">(current)</span></a>
                        </li>

                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Menu
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                @foreach($menues AS $menu)
                                
                                 <a class="dropdown-item" href="{{route("menu")}}/{{$menu->id}}">{{$menu->name}}</a>
                                
                                @endforeach
                            </div>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Contatti</a>
                        </li>
                    </ul>    
                </div>
            </div>
        </nav>

        @yield('content')    



        <footer class="mt-2 pt-5 pb-5">
            
            <div class="container">
                <div class="row">
                <div class="col">
                    <a href="{{route("home")}}"><img src="{{asset("img/logo.png")}}" class="w-25"></a><br>
                    {!!$setting->description!!}
                </div>
                <div class="col">
                    
                    {!!$setting->timetable!!}
                    
                </div>
                <div class="col text-right">
                    <a href="{{route("home")}}">Home</a>
                    <a href="{{route("home")}}">Contatti</a>
                    @foreach($menues AS $menu)                                
                        <a href="{{route("menu")}}/{{$menu->slug}}">{{$menu->name}}</a>                                
                    @endforeach                    
                                
                    <a href="https://www.iubenda.com/privacy-policy/{{$setting->iubenda_code}}" class="iubenda-nostyle iubenda-embed" title="Privacy Policy ">Privacy Policy</a>
                            <script type="text/javascript">(function (w, d) {
                            var loader = function () {
                                var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0];
                                s.src = "https://cdn.iubenda.com/iubenda.js";
                                tag.parentNode.insertBefore(s, tag);
                            };
                            if (w.addEventListener) {
                                w.addEventListener("load", loader, false);
                            } else if (w.attachEvent) {
                                w.attachEvent("onload", loader);
                            } else {
                                w.onload = loader;
                            }
                        })(window, document);</script>
                    <a href="https://www.iubenda.com/privacy-policy/{{$setting->iubenda_code}}/cookie-policy" class="iubenda-nostyle iubenda-embed" title="Cookie Policy ">Cookie Policy</a><script type="text/javascript">(function (w, d) {
                            var loader = function () {
                                var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0];
                                s.src = "https://cdn.iubenda.com/iubenda.js";
                                tag.parentNode.insertBefore(s, tag);
                            };
                            if (w.addEventListener) {
                                w.addEventListener("load", loader, false);
                            } else if (w.attachEvent) {
                                w.attachEvent("onload", loader);
                            } else {
                                w.onload = loader;
                            }
                        })(window, document);</script>
                    <a href="{{route("termini-e-condizioni")}}">Termini e Condizioni</a>                    
                    
                </div>
                </div>
            </div>
            
            
        </footer>


    </body>


    <script type="text/javascript">
        var _iub = _iub || [];
        _iub.csConfiguration = {
            "lang": "it",
            "siteId": "{{$setting->iubenda_site_id}}", //usa il tuo siteId
            "cookiePolicyId": "{{$setting->iubenda_code}}", //usa il tuo cookiePolicyId
            "banner": {
                "position": "float-top-center",
                "acceptButtonDisplay": true,
                "customizeButtonDisplay": true
            }
        };
    </script>
    <script type="text/javascript" src="//cdn.iubenda.com/cs/iubenda_cs.js" charset="UTF-8" async></script>


        <!-- Bootstrap core JavaScript
       ================================================== -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>    
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

        <script src="{{asset("/js/parallax.min.js")}}" type="text/javascript"></script>
        <script>$('.parallax-window').parallax({imageSrc: '{{asset("img/banner.jpg")}}'});</script>


</html>