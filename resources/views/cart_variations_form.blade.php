<form id="async" method="POST" enctype="multipart/form-data" > 
    {{ csrf_field() }} 

    <div class="row">   
        <input type="hidden" name="id_product" value="{{ $product->id }}">
        <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Prezzo</th>

                        <th style="width: 1px"></th>
                        <th style="width: 1px"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
        
    </div>    
    <button class="d-none" id="click-me"></button>
</form>
