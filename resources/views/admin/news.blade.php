@extends('layouts.admin')

@section('js')

<script>
    
    function exchange(id1, id2){
        $.post('{{route('admin.news_exchange')}}', {id1:id1, id2:id2,  _token: "{{ csrf_token() }}"}, function(){
            table.ajax.reload(null, false);
        });
    }
    
    
    
    
    var table;
    $(document).ready(function () {
        
    $.fn.dataTable.moment('DD-MM-YYYY');
    table = $('#dataTable').DataTable({
 "ordering": false,
    language: {
    processing: "Caricamento in corso...",
            search: "Cerca:",
            lengthNews: "Mostra _MENU_ elementi",
            info: "Mostra elementi da _START_ a _END_ su _TOTAL_ ",
            infoEmpty: "Mostra elementi da 0 a 0 su 0",
            infoFiltered: "(filtro su _MAX_ elementi totali)",
            infoPostFix: "",
            loadingRecords: "Caricamento in corso...",
            zeroRecords: "Nessun elemento da mostrare",
            emptyTable: "Nessun elemento da mostrare",
            paginate: {
            first: "Primo",
                    previous: "Precedente",
                    next: "Seguente",
                    last: "Ultimo"
            },
            
    },
     
            "columnDefs": [
                
            {"searchable": false, "orderable": false, targets: 2},
            {"searchable": false, "orderable": false, targets: 3},
            {"searchable": false, "orderable": false, targets: 4},
            {"searchable": false, "orderable": false, targets: 5}

            ],
            
            
            
            "order": [[1, "asc"]],
            "ajax": {
            "url": "{{route('admin.news_json')}}",
                    "data": function (d){
                        d._token = "{{ csrf_token() }}"                        
                    },
                    "type": "POST"
            }
    });
    });</script>
@stop


@section('content')

<div class="card">
    <div class="card-header">
        <h3 class="card-title">News</h3>  
        
        
        <button data-backdrop="static"  data-toggle="modal" data-target="#Modal" onclick="$('#Modal .modal-body').load('{{route('admin.news_form')}}')" class="btn btn-primary btn-sm  btn-add float-right">aggiungi</button>
        
        
        
    </div>
    <!-- /.box-header -->
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Number</th>
                        <th>Nome</th>
                        <th style="width: 1px"></th>
                        <th style="width: 1px"></th>
                        <th style="width: 1px"></th>
                        <th style="width: 1px"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="Modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
                <button type="button" class="btn btn-primary" onclick="$(this).hide().delay(1000).show(0); $('#images').removeAttr('required'); $('#click-me').click(); $('#images').prop('required',true);">Conferma</button>
            </div>
        </div>
    </div>
</div>







@endsection
