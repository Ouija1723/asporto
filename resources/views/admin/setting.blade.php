@extends('layouts.admin')

@section('js')
<script>


    $(function () {
    ClassicEditor
            .create(document.querySelector('#text1'))
            .catch(error => {
            console.error(error);
            });
    ClassicEditor
            .create(document.querySelector('#text2'))
            .catch(error => {
            console.error(error);
            });
    ClassicEditor
            .create(document.querySelector('#text3'))
            .catch(error => {
            console.error(error);
            });
    $('#async').submit(function (e) {
    e.preventDefault();
    var formData = new FormData($(this)[0]);
    $.ajax({
    url: '{{route('admin.settings')}}',
            type: 'POST',
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data)
            {
            if (data.error) {

            $(document).Toasts('create', {
            title: 'Errore',
                    class: 'bg-danger',
                    autohide: true,
                    delay: 2000,
                    body: data.error
            })



            } else {
            $("#Modal").modal('hide');
            $("#Modal .close").click()
                    $(document).Toasts('create', {
            title: 'Successo',
                    class: 'bg-success',
                    autohide: true,
                    delay: 2000,
                    body: 'Dati inseriti correttamente'
            })

                    //table.ajax.reload(null, false);
            }
            }
    });
    });
    });
</script>
@endsection


@section('content')



<div class="card">
    <div class="card-header">
        <h3 class="card-title">Impostazioni generali</h3>
    </div>
    <!-- /.box-header -->
    <div class="card-body">


        <form id="async" method="POST" enctype="multipart/form-data" >


            {!! csrf_field() !!}
            <div class="row">

                <div class="col-6">
                    <label class="label label-primary">Nome<sup>*</sup></label>
                    <input class="form-control" type="text" name="name" value="{{$setting->name}}">
                    <input class="form-control" type="hidden" name="id" value="{{$setting->id}}">
                </div>             
                <div class="col-6">
                    <label class="label label-primary">Logo<sup>*</sup></label>
                    <input class="form-control" type="file" name="logo">
                </div>
                <div class="col-6">
                    <label class="label label-primary">Banner<sup>*</sup></label>
                    <input class="form-control" type="file" name="banner">
                </div>
                <div class="col-6">
                    <label class="label label-primary">Icon<sup>*</sup></label>
                    <input class="form-control" type="file" name="icon">
                </div>

                <div class="col-6">
                    <label class="label label-primary">Titolo sotto banner</label>
                    <input class="form-control" type="text" name="under_banner" value="{{$setting->under_banner}}">
                </div>
                <div class="col-6">
                    <label class="label label-primary">Titolo</label>
                    <input class="form-control" type="text" name="h1" value="{{$setting->h1}}">
                </div>
                <div class="col-6">
                    <label class="label label-primary">Sottotitolo</label>
                    <input class="form-control" type="text" name="h2" value="{{$setting->h2}}">
                </div>

                <div class="col-6">
                    <label class="label label-primary">Prezzo Delivery</label>
                    <input class="form-control" type="number" step="0.01" name="delivery_price" value="{{$setting->delivery_price}}">
                </div>
                <div class="col-6">
                    <label class="label label-primary">Prezzo Delivery Gratis a partire da</label>
                    <input class="form-control" type="number" step="0.01" name="delivery_price_min" value="{{$setting->delivery_price_min}}">
                </div>
                
                
                

                <div class="col-6">
                    <label class="label label-primary">Iubenda Site ID</label>
                    <input class="form-control" type="text" name="iubenda_site_id" value="{{$setting->iubenda_site_id}}">
                </div>
                <div class="col-6">
                    <label class="label label-primary">Iubenda Code</label>
                    <input class="form-control" type="text" name="iubenda_code" value="{{$setting->iubenda_code}}">
                </div>
                <div class="col-6">
                    <label class="label label-primary">Iubenda API Key</label>
                    <input class="form-control" type="text" name="iubenda_api_key" value="{{$setting->iubenda_api_key}}">
                </div>
                
                
                <div class="col-6">
                    <label class="label label-primary">Info Footer (es. indirizzo, P.IVA,...)</label>
                    <textarea class="form-control" name="description" id="text1">{{$setting->description}}</textarea>
                </div>
                <div class="col-6">
                    <label class="label label-primary">Orari Footer</label>
                    <textarea class="form-control" name="timetable" id="text2">{{$setting->timetable}}</textarea>
                </div>

                <div class="col-12">
                    <label class="label label-primary">Termini e condizioni</label>
                    <textarea class="form-control" name="conditions" id="text3">{{$setting->conditions}}</textarea>
                </div>




            </div>


            <button class="btn btn-primary mt-4 float-right">SALVA</button>




        </form>


    </div>
</div>

@endsection