<form id="async" method="POST" enctype="multipart/form-data" >


    {!! csrf_field() !!}
    <div class="row">

        <div class="col-12">
            <label class="label label-primary">Nome</label>
            <input class="form-control" type="text" name="name" value="{{$item->name}}" required>
            <input class="form-control" type="hidden" name="id" value="{{$item->id}}">
            
        </div>
        
        <div class="col-12">
            <label class="label label-primary">Tipo</label>
            <select class="form-control js-select" name="delivery"> 
            
            
                <option value="0" @if(0==$item->delivery) selected @endif>MENU VIRTUALE</option>
                <option value="1" @if(1==$item->delivery) selected @endif>TAKE AWAY</option>
            
            
            </select>
        </div>    
        
        
    </div>


    <button class="d-none" id="click-me"></button>
</form>


<script>


    $(function () {
        

        $('#async').submit(function (e) {
            e.preventDefault();

            var formData = new FormData($(this)[0]);
            $.ajax({
                url: '{{route('admin.menues_form')}}',
                type: 'POST',
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data)
                {
                    if (data.error) {

                        $(document).Toasts('create', {
                            title: 'Errore',
                            class: 'bg-danger',
                            autohide: true,
                            delay: 2000,
                            body: data.error
                        })



                    } else {
                        $("#Modal").modal('hide');
                        $("#Modal .close").click()
                        $(document).Toasts('create', {
                            title: 'Successo',
                            class: 'bg-success',
                            autohide: true,
                            delay: 2000,
                            body: 'Dati inseriti correttamente'
                        })

                        table.ajax.reload(null, false);
                    }
                }
            });
        });
    });
</script>