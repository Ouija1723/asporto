<form id="async" method="POST" enctype="multipart/form-data" >


    {!! csrf_field() !!}
    <div class="row">

        <div class="col-12">
            <label class="label label-primary">Giorno</label>
            <select class="form-control" name="day">
                
                @foreach($days AS $key => $day)
                
                <option value='{{$key}}' @if($item->day==$key) selected @endif>{{$day}}</option>
                
                @endforeach
                
            </select>    
            <input class="form-control" type="hidden" name="id" value="{{$item->id}}">
            
        </div>
        
        <div class="col-6">
            <label class="label label-primary">Ora inizio</label>
            <input class="form-control" type="time" name="begin" value="{{$item->begin}}">
        </div>    
        <div class="col-6">
            <label class="label label-primary">Ora fine</label>
            <input class="form-control" type="time" name="end" value="{{$item->end}}">
        </div>    
        
    </div>


    <button class="d-none" id="click-me"></button>
</form>


<script>


    $(function () {
        

        $('#async').submit(function (e) {
            e.preventDefault();

            var formData = new FormData($(this)[0]);
            $.ajax({
                url: '{{route('admin.timetables_form')}}',
                type: 'POST',
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data)
                {
                    if (data.error) {

                        $(document).Toasts('create', {
                            title: 'Errore',
                            class: 'bg-danger',
                            autohide: true,
                            delay: 2000,
                            body: data.error
                        })



                    } else {
                        $("#Modal").modal('hide');
                        $("#Modal .close").click()
                        $(document).Toasts('create', {
                            title: 'Successo',
                            class: 'bg-success',
                            autohide: true,
                            delay: 2000,
                            body: 'Dati inseriti correttamente'
                        })

                        table.ajax.reload(null, false);
                    }
                }
            });
        });
    });
</script>