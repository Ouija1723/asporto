<form id="async" method="POST" enctype="multipart/form-data" >


    {!! csrf_field() !!}
    <div class="row">

        <div class="col-12">
            <label class="label label-primary">Nome</label>
            <input class="form-control" type="text" name="name" value="{{$item->name}}" required>
            <input class="form-control" type="hidden" name="id" value="{{$item->id}}">
            
        </div>
        
        <div class="col-6">
            <label class="label label-primary">Modifica prezzo per aggiunta (+)</label>
            <input class="form-control" type="number" step="0.01" min="0" name="price_add" value="{{$item->price_add}}">
        </div>    
        <div class="col-6">
            <label class="label label-primary">Modifica prezzo per rimozione (-)</label>
            <input class="form-control" type="number" step="0.01" min="0" name="price_remove" value="{{$item->price_remove}}">
        </div>    
        
    </div>


    <button class="d-none" id="click-me"></button>
</form>


<script>


    $(function () {
        

        $('#async').submit(function (e) {
            e.preventDefault();

            var formData = new FormData($(this)[0]);
            $.ajax({
                url: '{{route('admin.addons_form')}}',
                type: 'POST',
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data)
                {
                    if (data.error) {

                        $(document).Toasts('create', {
                            title: 'Errore',
                            class: 'bg-danger',
                            autohide: true,
                            delay: 2000,
                            body: data.error
                        })



                    } else {
                        $("#Modal").modal('hide');
                        $("#Modal .close").click()
                        $(document).Toasts('create', {
                            title: 'Successo',
                            class: 'bg-success',
                            autohide: true,
                            delay: 2000,
                            body: 'Dati inseriti correttamente'
                        })

                        table.ajax.reload(null, false);
                    }
                }
            });
        });
    });
</script>