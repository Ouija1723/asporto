<form id="async" method="POST" enctype="multipart/form-data" >


    {!! csrf_field() !!}
    <div class="row">
        <div class="col-12">
            <label class="label label-primary">Titolo</label>
            <input class="form-control" type="text" name="title" value="{{$item->title}}" required>
            <input class="form-control" type="hidden" name="id" value="{{$item->id}}">            
        </div>
        
        <div class="col-12">
            <label class="label label-primary">Foto</label>
            <input class="form-control" type="file" name="image">                     
        </div>
        
        
        
        <div class="col-12">
            <label class="label label-primary">Sottotitolo</label>
            <input class="form-control" type="text" name="sutitle" value="{{$item->sutitle}}" required>                     
        </div>
        
        <div class="col-12">
            <label class="label label-primary">Testo</label>
            <textarea class="form-control" name="text" id="text">{{$item->text}}</textarea>
        </div>
        
        
        
    </div>


    <button class="d-none" id="click-me"></button>
</form>


<script>


    $(function () {
        
        ClassicEditor
        .create( document.querySelector( '#text' ) )
        .catch( error => {
            console.error( error );
        } );
        
        

        $('#async').submit(function (e) {
            e.preventDefault();

            var formData = new FormData($(this)[0]);
            $.ajax({
                url: '{{route('admin.news_form')}}',
                type: 'POST',
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data)
                {
                    if (data.error) {

                        $(document).Toasts('create', {
                            title: 'Errore',
                            class: 'bg-danger',
                            autohide: true,
                            delay: 2000,
                            body: data.error
                        })



                    } else {
                        $("#Modal").modal('hide');
                        $("#Modal .close").click()
                        $(document).Toasts('create', {
                            title: 'Successo',
                            class: 'bg-success',
                            autohide: true,
                            delay: 2000,
                            body: 'Dati inseriti correttamente'
                        })

                        table.ajax.reload(null, false);
                    }
                }
            });
        });
    });
</script>