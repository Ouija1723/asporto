<form id="async" method="POST" enctype="multipart/form-data" >


    {!! csrf_field() !!}
    <div class="row">
        <div class="col-12">
            <label class="label label-primary">Titolo</label>
            <input class="form-control" type="text" name="title" value="{{$item->title}}" required>
            <input class="form-control" type="hidden" name="id" value="{{$item->id}}">            
        </div>

        <div class="col-12">
            <label class="label label-primary">Immagine</label>
            <input class="form-control" type="file" name="image">                     
        </div>



        <div class="col-12">
            <label class="label label-primary">Descrizione Breve</label>
            <input class="form-control" type="text" name="sutitle" value="{{$item->sutitle}}">                     
        </div>

        <div class="col-12">
            <label class="label label-primary">Descrizione Lunga</label>
            <textarea class="form-control" name="text" id="text">{{$item->text}}</textarea>
        </div>

        <div class="col-12">
            <label class="label label-primary">Price</label>
            <input class="form-control" type="number" step='0.01' name="price" value="{{$item->price}}">        
        </div>
    </div>


    <div class='row'>
        <div class='col'>
        <a class="btn btn-primary mt-3 mb-3" data-toggle="collapse" href="#collapseMenu" role="button" aria-expanded="false" aria-controls="collapseMenu">
            Menu
        </a>
        </div>
    </div>
    <div class="collapse" id="collapseMenu">
        <div class='row'>

            @foreach($menues As $menu)

            <div class='col-3'><input type='checkbox' name='menu[]' value='{{$menu->id}}' @if($item->menues->contains($menu->id)) checked @endif> {{$menu->name}}</div>


            @endforeach

        </div>
    </div>
    <div class='row'>
<div class='col'>
        <a class="btn btn-primary mt-3 mb-3" data-toggle="collapse" href="#collapseCategory" role="button" aria-expanded="false" aria-controls="collapseCategory">
            Categorie
        </a>
</div>
    </div>
    <div class="collapse" id="collapseCategory">
        <div class='row'>

            @foreach($categories As $category)

            <div class='col-3'><input type='checkbox' name='category[]' value='{{$category->id}}' @if($item->categories->contains($category->id)) checked @endif> {{$category->name}}</div>


            @endforeach

        </div>
    </div>
    <div class='row'>
<div class='col'>
        <a class="btn btn-primary mt-3 mb-3" data-toggle="collapse" href="#collapseVgroup" role="button" aria-expanded="false" aria-controls="collapseCategory">
            Gruppi di Variazioni
        </a>
</div>
    </div>
    <div class="collapse" id="collapseVgroup">
        <div class='row'>

            @foreach($vgroups As $vgroup)

            <div class='col-3'><input type='checkbox' name='vgroup[]' value='{{$vgroup->id}}' @if($item->vgroups->contains($vgroup->id)) checked @endif> {{$vgroup->name}}</div>


            @endforeach

        </div>
    </div>

    <div class='row'>
<div class='col'>
        <a class="btn btn-primary mt-3 mb-3" data-toggle="collapse" href="#collapseAddons" role="button" aria-expanded="false" aria-controls="collapseCategory">
            Aggiunte
        </a>
</div>
    </div>
    <div class="collapse" id="collapseAddons">
        <div class='row'>

            @foreach($addons As $addon)

            <div class='col-3'><input type='checkbox' name='addon[]' value='{{$addon->id}}' @if($item->addons->contains($addon->id)) checked @endif> {{$addon->name}}</div>


            @endforeach

        </div>
    </div>
    
        <div class='row'>
<div class='col'>
        <a class="btn btn-primary mt-3 mb-3" data-toggle="collapse" href="#collapseRemons" role="button" aria-expanded="false" aria-controls="collapseCategory">
            Rimozioni
        </a>
</div>
    </div>
    <div class="collapse" id="collapseRemons">
        <div class='row'>

            @foreach($addons As $addon)

            <div class='col-3'><input type='checkbox' name='remon[]' value='{{$addon->id}}' @if($item->remons->contains($addon->id)) checked @endif> {{$addon->name}}</div>


            @endforeach

        </div>
    </div>



    <button class="d-none" id="click-me"></button>
</form>


<script>


    $(function () {

    ClassicEditor
            .create(document.querySelector('#text'))
            .catch(error => {
            console.error(error);
            });
    $('#async').submit(function (e) {
    e.preventDefault();
    var formData = new FormData($(this)[0]);
    $.ajax({
    url: '{{route('admin.products_form')}}',
            type: 'POST',
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data)
            {
            if (data.error) {

            $(document).Toasts('create', {
            title: 'Errore',
                    class: 'bg-danger',
                    autohide: true,
                    delay: 2000,
                    body: data.error
            })



            } else {
            $("#Modal").modal('hide');
            $("#Modal .close").click()
                    $(document).Toasts('create', {
            title: 'Successo',
                    class: 'bg-success',
                    autohide: true,
                    delay: 2000,
                    body: 'Dati inseriti correttamente'
            })

                    table.ajax.reload(null, false);
            }
            }
    });
    });
    });
</script>