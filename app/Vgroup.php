<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vgroup extends Model
{
    protected $fillable = ['name','importance'];
    
    public function variations()
    {
        return $this->hasMany('App\Variation');
    }
    
}
