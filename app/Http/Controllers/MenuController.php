<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use Str;
use Illuminate\Support\Facades\Validator;
class MenuController extends Controller {

    /* Ritorna la view menues */
    public function index() {
        return view('admin.menues');
    }

    /* Se la request ha un id
    nella variabile $item metto il menu corrispettivo a quell'id
    ed estraggo l'id da $item e lo metto in $id
    altrimenti
    creo un nuovo menu con id 0
    Poi controlla che il name sia inserito e unico
    Se il validatore fallisce, visualizza il messaggio d'errore
    altrimenti
    il nome del menu $item diventa il nome inserito nella request
    la slug del menu $item diventa la slug inserita nella request
    la delivery del menu $item diventa la delivery nella request
    Salvo $item */
    public function store(Request $request) {
        if ($request->id) {
            $item = Menu::find($request->id);

            $id = $item->id;
        } else {
            $item = new Menu;
            $id = 0;
            $item->importance = time();
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:menues,name,' . $id,
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        } else {
            $item->name = $request->name;
            $item->slug = Str::slug($request->name);
            $item->delivery = $request->delivery;

            $item->save();
        }
    }

    /* Cerca il menu che si vuole modificare altrimenti lo crea
    Restituisce la view menues_form con parametro il menu trovato */
    public function edit($id=0) {
        if ($id) {
            $item = Menu::find($id);
        } else {
            $item = new Menu;
        }



        return view('admin.menues_form', [
            'item' => $item,
        ]);
    }

    /* OBIETTIVO ACQUISITO
    INGAGGIO
    BERSAGLIO ELIMINATO */
    public function destroy(Request $request) {
        $item = Menu::find($request->id_to_del);
        $item->delete();
    }

    /* Gestisce la visualizzazione dei dati da datatables
    crei un unico array e lo metti in un array */
    public function json(Request $request) {

        $items = Menu::orderby("importance")->get();

        $data["data"] = array();

        for($i=0; $i<count($items); $i++){

            $array = array();

            if($i>0)
                $prev=$items[($i-1)];


            $item=$items[$i];

            if($i<(count($items)-1))
                $next=$items[($i+1)];


            $array[] = $i+1;
            $array[] = $item->name;


            if($i>0)
                $array[]='<button onclick="exchange('.$item->id.','.$prev->id.')" class="btn btn-primary btn-sm  btn-add"><i class="fas fa-chevron-up"></i></button>';
            else
                $array[]='';

            if($i<(count($items)-1))
                $array[]='<button onclick="exchange('.$item->id.','.$next->id.')" class="btn btn-primary btn-sm  btn-add"><i class="fas fa-chevron-down"></i></button>';
            else
                $array[]='';


            $array[] = '<button data-backdrop="static"  data-toggle="modal" data-target="#Modal" onclick="$(\'#Modal .modal-body\').load(\'' . route('admin.menues_form') . '/' . $item->id . '\')" class="btn btn-primary btn-sm  btn-add">modifica</button>';
            $array[] = '<button onclick="if(confirm(\'Conferma eliminazione?\')){$.post(\'' . route('admin.menues_destroy') . '\', {id_to_del:' . $item->id . ', _token: \'' . csrf_token() . '\'}, function(){table.ajax.reload( null, false );});  }" class="btn btn-primary btn-sm  btn-add">elimina</button>';

            $data["data"][] = $array;
        }
        echo json_encode($data);
    }

    /* uno swap di posto nell'elenco dei menu */
    public function exchange(Request $request){

        $item1= Menu::find($request->id1);
        $item2= Menu::find($request->id2);

        $importance1=$item1->importance;
        $importance2=$item2->importance;

        $item1->importance=$importance2;
        $item2->importance=$importance1;

        $item1->save();
        $item2->save();

    }



}
