<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Printarea;
use Illuminate\Support\Facades\Validator;


class PrintAreaController extends Controller
{
    public function index() {
        return view('admin.printareas');
    }

    public function store(Request $request) {
        if ($request->id) {
            $item = PrintArea::find($request->id);

            $id = $item->id;
        } else {
            $item = new PrintArea;
            $id = 0;
            $item->importance = time();
        }

        $validator = Validator::make($request->all(), [
            'luogo' => 'required',
            'ip' => 'required',
            'copies'=> 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        } else {
            $item->ip = $request->ip;
            $item->luogo = $request->luogo;
            $item->copies = $request->copies;
            $item->save();
        }
    }

    public function edit($id=0) {
        if ($id) {
            $item = PrintArea::find($id);
        } else {
            $item = new PrintArea;
        }



        return view('admin.printareas_form', [
            'item' => $item,
        ]);
    }

    public function destroy(Request $request) {
        $item = PrintArea::find($request->id_to_del);
        $item->delete();
    }

    public function json(Request $request) {

        $items = PrintArea::orderby("importance")->get();

        $data["data"] = array();

        for($i=0; $i<count($items); $i++){

            $array = array();

            if($i>0)
                $prev=$items[($i-1)];


            $item=$items[$i];

            if($i<(count($items)-1))
                $next=$items[($i+1)];


            $array[] = $i+1;
            $array[] = $item->luogo;
            $array[] = $item->ip;
            $array[] = $item->copies;


            if($i>0)
                $array[]='<button onclick="exchange('.$item->id.','.$prev->id.')" class="btn btn-primary btn-sm  btn-add"><i class="fas fa-chevron-up"></i></button>';
            else
                $array[]='';

            if($i<(count($items)-1))
                $array[]='<button onclick="exchange('.$item->id.','.$next->id.')" class="btn btn-primary btn-sm  btn-add"><i class="fas fa-chevron-down"></i></button>';
            else
                $array[]='';


            $array[] = '<button data-backdrop="static"  data-toggle="modal" data-target="#Modal" onclick="$(\'#Modal .modal-body\').load(\'' . route('admin.printareas_form') . '/' . $item->id . '\')" class="btn btn-primary btn-sm  btn-add">modifica</button>';
            $array[] = '<button onclick="if(confirm(\'Conferma eliminazione?\')){$.post(\'' . route('admin.printareas_destroy') . '\', {id_to_del:' . $item->id . ', _token: \'' . csrf_token() . '\'}, function(){table.ajax.reload( null, false );});  }" class="btn btn-primary btn-sm  btn-add">elimina</button>';

            $data["data"][] = $array;
        }
        echo json_encode($data);
    }

    public function exchange(Request $request){

        $item1= PrintArea::find($request->id1);
        $item2= PrintArea::find($request->id2);

        $importance1=$item1->importance;
        $importance2=$item2->importance;

        $item1->importance=$importance2;
        $item2->importance=$importance1;

        $item1->save();
        $item2->save();

    }

}
