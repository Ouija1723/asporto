<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use Str;
use Illuminate\Support\Facades\Validator;
class NewsController extends Controller {

    /* ritorna la view news */
    public function index() {
        return view('admin.news');
    }

    /* Se la request contiene un id, allora la variabile $item diventa la news con quell'id
    crea la variabile $id e ci mette dentro l'id di $item
    Altrimenti
    crea una nuova news e la mette in $item, con id 0 e importance istantanea
    fillo $item con gli elementi della request
    salvo $item
    Se la request contiene un'immagine
    la variabile $name le viene assegnata con (---)
    muove l'immagine nella cartella public per renderla visibile al pubblico
    la mette in $item e la salva */
    public function store(Request $request) {
        if ($request->id) {
            $item = News::find($request->id);

            $id = $item->id;
        } else {
            $item = new News;
            $id = 0;
            $item->importance = time();
        }


            $item->fill($request->all());




            $item->save();

            if($request->image){
                $name=Str::slug($request->title)."-".$item->id.".jpg";
                $request->image->move(public_path('news'), $name);
                $item->img = $name;
                $item->save();
            }




    }

    public function edit($id=0) {
        if ($id) {
            $item = News::find($id);
        } else {
            $item = new News;
        }



        return view('admin.news_form', [
            'item' => $item,
        ]);
    }

    public function destroy(Request $request) {
        $item = News::find($request->id_to_del);
        $item->delete();
    }

    public function json(Request $request) {

        $items = News::orderby("importance")->get();

        $data["data"] = array();

        for($i=0; $i<count($items); $i++){

            $array = array();

            if($i>0)
                $prev=$items[($i-1)];


            $item=$items[$i];

            if($i<(count($items)-1))
                $next=$items[($i+1)];


            $array[] = $i+1;
            $array[] = $item->title;


            if($i>0)
                $array[]='<button onclick="exchange('.$item->id.','.$prev->id.')" class="btn btn-primary btn-sm  btn-add"><i class="fas fa-chevron-up"></i></button>';
            else
                $array[]='';

            if($i<(count($items)-1))
                $array[]='<button onclick="exchange('.$item->id.','.$next->id.')" class="btn btn-primary btn-sm  btn-add"><i class="fas fa-chevron-down"></i></button>';
            else
                $array[]='';


            $array[] = '<button data-backdrop="static"  data-toggle="modal" data-target="#Modal" onclick="$(\'#Modal .modal-body\').load(\'' . route('admin.news_form') . '/' . $item->id . '\')" class="btn btn-primary btn-sm  btn-add">modifica</button>';
            $array[] = '<button onclick="if(confirm(\'Conferma eliminazione?\')){$.post(\'' . route('admin.news_destroy') . '\', {id_to_del:' . $item->id . ', _token: \'' . csrf_token() . '\'}, function(){table.ajax.reload( null, false );});  }" class="btn btn-primary btn-sm  btn-add">elimina</button>';

            $data["data"][] = $array;
        }
        echo json_encode($data);
    }

    public function exchange(Request $request){

        $item1= News::find($request->id1);
        $item2= News::find($request->id2);

        $importance1=$item1->importance;
        $importance2=$item2->importance;

        $item1->importance=$importance2;
        $item2->importance=$importance1;

        $item1->save();
        $item2->save();

    }



}
