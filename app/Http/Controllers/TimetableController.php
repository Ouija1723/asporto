<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Timetable;
use Str;
use Illuminate\Support\Facades\Validator;
class TimetableController extends Controller {

    public function index() {
        return view('admin.timetables');
    }

    public function store(Request $request) {
        if ($request->id) {
            $item = Timetable::find($request->id);

            $id = $item->id;
        } else {
            $item = new Timetable;
            $id = 0;
           
        }

        
            $item->fill($request->all());
            $item->save();
            
    }

    public function edit($id=0) {
        if ($id) {
            $item = Timetable::find($id);
        } else {
            $item = new Timetable;
        }



        return view('admin.timetables_form', [
            'item' => $item,
            'days'=> Timetable::days(),
            
        ]);
    }

    public function destroy(Request $request) {
        $item = Timetable::find($request->id_to_del);
        $item->delete();
    }

    public function json(Request $request) {

        $items = Timetable::orderby("day")->orderby("begin")->get();

        $data["data"] = array();
        
        $days = Timetable::days();
        
        for($i=0; $i<count($items); $i++){
            
            $array = array();
            
            
            $item=$items[$i];
            
            
            
            $array[] = $days[$item->day];
            $array[] = $item->begin;
            $array[] = $item->end;
            
            
            $array[] = '<button data-backdrop="static"  data-toggle="modal" data-target="#Modal" onclick="$(\'#Modal .modal-body\').load(\'' . route('admin.timetables_form') . '/' . $item->id . '\')" class="btn btn-primary btn-sm  btn-add">modifica</button>';
            $array[] = '<button onclick="if(confirm(\'Conferma eliminazione?\')){$.post(\'' . route('admin.timetables_destroy') . '\', {id_to_del:' . $item->id . ', _token: \'' . csrf_token() . '\'}, function(){table.ajax.reload( null, false );});  }" class="btn btn-primary btn-sm  btn-add">elimina</button>';

            $data["data"][] = $array;
        }
        echo json_encode($data);
    }
    
    public function exchange(Request $request){        
        
        $item1= Timetable::find($request->id1);
        $item2= Timetable::find($request->id2);
        
        $importance1=$item1->importance;
        $importance2=$item2->importance;
        
        $item1->importance=$importance2;
        $item2->importance=$importance1;
        
        $item1->save();
        $item2->save();
    
    } 
    
    

}
