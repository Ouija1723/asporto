<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Addon;
use Str;
use Illuminate\Support\Facades\Validator;
class AddonController extends Controller {

    public function index() {
        return view('admin.addons');
    }

    public function store(Request $request) {
        if ($request->id) {
            $item = Addon::find($request->id);

            $id = $item->id;
        } else {
            $item = new Addon;
            $id = 0;
            $item->importance = time();
        }


            $item->fill($request->all());
            $item->save();

    }

    public function edit($id=0) {
        if ($id) {
            $item = Addon::find($id);
        } else {
            $item = new Addon;
        }



        return view('admin.addons_form', [
            'item' => $item,
        ]);
    }

    public function destroy(Request $request) {
        $item = Addon::find($request->id_to_del);
        $item->delete();
    }

    public function json(Request $request) {

        $items = Addon::orderby("importance")->get();

        $data["data"] = array();

        for($i=0; $i<count($items); $i++){

            $array = array();

            if($i>0)
                $prev=$items[($i-1)];


            $item=$items[$i];

            if($i<(count($items)-1))
                $next=$items[($i+1)];


            $array[] = $i+1;
            $array[] = $item->name;
            $array[] = $item->price_add;
            $array[] = $item->price_remove;


            if($i>0)
                $array[]='<button onclick="exchange('.$item->id.','.$prev->id.')" class="btn btn-primary btn-sm  btn-add"><i class="fas fa-chevron-up"></i></button>';
            else
                $array[]='';

            if($i<(count($items)-1))
                $array[]='<button onclick="exchange('.$item->id.','.$next->id.')" class="btn btn-primary btn-sm  btn-add"><i class="fas fa-chevron-down"></i></button>';
            else
                $array[]='';


            $array[] = '<button data-backdrop="static"  data-toggle="modal" data-target="#Modal" onclick="$(\'#Modal .modal-body\').load(\'' . route('admin.addons_form') . '/' . $item->id . '\')" class="btn btn-primary btn-sm  btn-add">modifica</button>';
            $array[] = '<button onclick="if(confirm(\'Conferma eliminazione?\')){$.post(\'' . route('admin.addons_destroy') . '\', {id_to_del:' . $item->id . ', _token: \'' . csrf_token() . '\'}, function(){table.ajax.reload( null, false );});  }" class="btn btn-primary btn-sm  btn-add">elimina</button>';

            $data["data"][] = $array;
        }
        echo json_encode($data);
    }

    public function exchange(Request $request){

        $item1= Addon::find($request->id1);
        $item2= Addon::find($request->id2);

        $importance1=$item1->importance;
        $importance2=$item2->importance;

        $item1->importance=$importance2;
        $item2->importance=$importance1;

        $item1->save();
        $item2->save();

    }



}
