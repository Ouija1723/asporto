<?php

namespace App\Http\Controllers;
use App\Setting;
use App\Menu;
use App\News;
use App\Category;
use App\Product;
use App\Variation;
use App\Vgroup;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        return view('home',[

            "setting" => Setting::find(1),
            "menues" => Menu::orderby("importance")->get(),
            "news" => News::orderby("importance")->get(),
        ]);
    }

    public function termini()
    {
       return view('termini',[
            "setting" => Setting::find(1),
            "menues" => Menu::orderby("importance")->get()
        ]);
    }

//////////////////FABIO//////////////////////
    public function menu($id)
    {
        $menu;
        if($id){
            $menu = Menu::find($id);
            $products = $menu->products;
            $menu_categories = array();
            $category_products = array();
            foreach ($products as $product) {
                if($product->price==null) $product->price= 0;
                $product_categories = $product->categories;
                foreach ($product_categories as $pc) {
                    if(!in_array($pc->name,$menu_categories)){
                        $menu_categories[] = $pc->name;
                    }
                }
            }
            foreach ($menu_categories as $mc) {
                $array = array();
                foreach ($products as $p) {
                    $categories = $p->categories;

                    foreach ($categories as $c) {
                        if($mc === $c->name){
                            $array[] = $p;
                        }
                    }
                }
                $category_products[$mc] = $array;
            }
        }
        return view('nostro_menu',[

            "setting" => Setting::find(1),
            "menues" => Menu::orderby("importance")->get(),
            "menu" => $menu,
            "products" => $products,
            "menu_categories" => $menu_categories,
            "category_products" => $category_products
            //"news" => News::orderby("importance")->get(),
        ]);
    }

    public function cart_variations_form($id)
    {
        $product = Product::find($id);
        return view('cart_variations_form',['product'=>$product]);
    }

    public function cart_variations_json($id)
    {
        $product = Product::find($id);
        $variations = array();
        $vgropus = $product->vgroups;
        $data["data"] = array();
        foreach ($vgropus as $vg) {
            $variations[$vg->name] = array();
            $variations[$vg->name] = Variation::where('vgroup_id',$vg->id);
            $vg_variations = Variation::where('vgroup_id',$vg->id);
            foreach ($vg_variations as $vg_v) {
                $array = array();
                $array[] = $vg_v->name;

            }

        }
    }

}
