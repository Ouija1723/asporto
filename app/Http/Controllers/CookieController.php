<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cookie;

class CookieController extends Controller
{
    public function esempio()
    {
        return Cookie::get('name');
    }

    public function esempio1()
    {
        Cookie::queue('name', $id, 60);
        return response('set cookie');
    }
}
