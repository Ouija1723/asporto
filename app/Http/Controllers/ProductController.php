<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Vgroup;
use App\Category;
use App\Menu;
use App\Addon;
use Str;

class ProductController extends Controller {

    public function index() {
        return view('admin.products');
    }

    public function store(Request $request) {
        if ($request->id) {
            $item = Product::find($request->id);

            $id = $item->id;
        } else {
            $item = new Product;
            $id = 0;
            $item->importance = time();
        }


            $item->fill($request->all()); // questa istruzione dovrebbe salvare anche il prezzo del prodotto, ma è stato necessario aggiungere la successiva istruzione.

            $item->price = $request->price;// FABIO

            $item->save();


            $item->menues()->sync($request->menu);
            $item->categories()->sync($request->category);
            $item->vgroups()->sync($request->vgroup);
            $item->addons()->sync($request->addon);
            $item->remons()->sync($request->remon);



            if($request->image){
                $name=Str::slug($request->title)."-".$item->id.".jpg";
                $request->image->move(public_path('products'), $name);
                $item->img = $name;
                $item->save();
            }




    }

    public function edit($id=0) {
        if ($id) {
            $item = Product::find($id);
        } else {
            $item = new Product;
        }



        return view('admin.products_form', [
            'item' => $item,
            'menues' => Menu::orderby("name")->get(),
            'categories' => Category::orderby("name")->get(),
            'addons' => Addon::orderby("name")->get(),
            'vgroups' => Vgroup::orderby("name")->get(),
        ]);
    }

    public function destroy(Request $request) {
        $item = Product::find($request->id_to_del);
        $item->delete();
    }

    public function json(Request $request) {

        $items = Product::orderby("importance")->get();

        $data["data"] = array();

        for($i=0; $i<count($items); $i++){

            $array = array();

            if($i>0)
                $prev=$items[($i-1)];


            $item=$items[$i];

            if($i<(count($items)-1))
                $next=$items[($i+1)];


            $array[] = $i+1;
            $array[] = $item->title;


            if($i>0)
                $array[]='<button onclick="exchange('.$item->id.','.$prev->id.')" class="btn btn-primary btn-sm  btn-add"><i class="fas fa-chevron-up"></i></button>';
            else
                $array[]='';

            if($i<(count($items)-1))
                $array[]='<button onclick="exchange('.$item->id.','.$next->id.')" class="btn btn-primary btn-sm  btn-add"><i class="fas fa-chevron-down"></i></button>';
            else
                $array[]='';


            $array[] = '<button data-backdrop="static"  data-toggle="modal" data-target="#Modal" onclick="$(\'#Modal .modal-body\').load(\'' . route('admin.products_form') . '/' . $item->id . '\')" class="btn btn-primary btn-sm  btn-add">modifica</button>';
            $array[] = '<button onclick="if(confirm(\'Conferma eliminazione?\')){$.post(\'' . route('admin.products_destroy') . '\', {id_to_del:' . $item->id . ', _token: \'' . csrf_token() . '\'}, function(){table.ajax.reload( null, false );});  }" class="btn btn-primary btn-sm  btn-add">elimina</button>';

            $data["data"][] = $array;
        }
        echo json_encode($data);
    }

    public function exchange(Request $request){

        $item1= Product::find($request->id1);
        $item2= Product::find($request->id2);

        $importance1=$item1->importance;
        $importance2=$item2->importance;

        $item1->importance=$importance2;
        $item2->importance=$importance1;

        $item1->save();
        $item2->save();

    }



}
