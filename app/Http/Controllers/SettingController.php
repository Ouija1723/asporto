<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Setting;
use Illuminate\Support\Facades\Validator;
class SettingController extends Controller
{
    /* Trova dalla tabella setting gli elementi con id 1 (???) e lo incapsula nella variabile $item
    Se non ne esistono, ne crea uno
    Restituisce la view setting passandole un array con dentro all'indice "setting" il contenuto di $item */
    public function index()
    {

        $item = Setting::find(1);
if(!$item){

    $item = new Setting;

}
        return view('admin.setting', [
            'setting' => $item
        ]);
    }

    /* Trova dalla tab setting, elementi con id 1 ??? e lo incapsula nella variabile $item
    Se non ne trova, lo crea
    Controlla che l'utente abbiainserito il nome nell'apposito campo
    Se la validazione non è confermata, restituisce il testo di errore
    altrimenti, se sono presenti, sposta logo, banner e icona nella cartella public (di modo che siano visibili ai clienti)
    salva $item */
    public function save(Request $request)
    {
        $item = Setting::find(1);

        if(!$item){
            Setting::create();
            $item = Setting::find(1);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        } else {
            $item->fill($request->all());

            if($request->logo){
                $name="logo.png";
                $request->logo->move(public_path('img'), $name);
            }
            if($request->banner){
                $name="banner.jpg";
                $request->banner->move(public_path('img'), $name);
            }
            if($request->icon){
                $name="icon.png";
                $request->icon->move(public_path('img'), $name);
            }

            $item->save();
        }
    }
}
