<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menues';
    
    public function products(){
        return $this->belongsToMany('App\Product');
    }
}
