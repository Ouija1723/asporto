<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['title','sutitle','text','importance'];

    public function menues()
    {
        return $this->belongsToMany('App\Menu');
    }

    public function vgroups()
    {
        return $this->belongsToMany('App\Vgroup');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function addons()
    {
        return $this->belongsToMany('App\Addon');
    }

    public function remons()
    {
        return $this->belongsToMany('App\Addon','remon_product');
    }

    public function printareas()
    {
        return $this->belongsToMany('App\PrintArea');
    }

}
