<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Printarea extends Model
{
    protected $fillable = ['ip','importance','luogo', 'copies'];
}
