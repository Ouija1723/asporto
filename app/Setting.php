<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['name','under_banner','h1','h2','iubenda_site_id','iubenda_code','iubenda_api_key','timetable','description','conditions','delivery_price','delivery_price_min'];
}
