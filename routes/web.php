<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::prefix('gestione')->name('admin.')->middleware('auth')->group(function () {
    Route::get('/', 'DashboardController@index')->name('home');

    Route::get('/settings', 'SettingController@index')->name('settings');
    Route::post('/settings', 'SettingController@save');

    Route::get('/menues', 'MenuController@index')->name('menues');
    Route::post('/menues/json', 'MenuController@json')->name('menues_json');
    Route::get('/menues/form', 'MenuController@edit')->name('menues_form');
    Route::get('/menues/form/{id}', 'MenuController@edit');
    Route::post('/menues/form', 'MenuController@store');
    Route::post('/menues/delete', 'MenuController@destroy')->name('menues_destroy');
    Route::post('/menues/exchange', 'MenuController@exchange')->name('menues_exchange');

    Route::get('/news', 'NewsController@index')->name('news');
    Route::post('/news/json', 'NewsController@json')->name('news_json');
    Route::get('/news/form', 'NewsController@edit')->name('news_form');
    Route::get('/news/form/{id}', 'NewsController@edit');
    Route::post('/news/form', 'NewsController@store');
    Route::post('/news/delete', 'NewsController@destroy')->name('news_destroy');
    Route::post('/news/exchange', 'NewsController@exchange')->name('news_exchange');

    Route::get('/categories', 'CategoryController@index')->name('categories');
    Route::post('/categories/json', 'CategoryController@json')->name('categories_json');
    Route::get('/categories/form', 'CategoryController@edit')->name('categories_form');
    Route::get('/categories/form/{id}', 'CategoryController@edit');
    Route::post('/categories/form', 'CategoryController@store');
    Route::post('/categories/delete', 'CategoryController@destroy')->name('categories_destroy');
    Route::post('/categories/exchange', 'CategoryController@exchange')->name('categories_exchange');

    Route::get('/printareas', 'PrintAreaController@index')->name('printareas');
    Route::post('/printareas/json', 'PrintAreaController@json')->name('printareas_json');
    Route::get('/printareas/form', 'PrintAreaController@edit')->name('printareas_form');
    Route::get('/printareas/form/{id}', 'PrintAreaController@edit');
    Route::post('/printareas/form', 'PrintAreaController@store');
    Route::post('/printareas/delete', 'PrintAreaController@destroy')->name('printareas_destroy');
    Route::post('/printareas/exchange', 'PrintAreaController@exchange')->name('printareas_exchange');


    Route::get('/vgroups', 'VgroupController@index')->name('vgroups');
    Route::post('/vgroups/json', 'VgroupController@json')->name('vgroups_json');
    Route::get('/vgroups/form', 'VgroupController@edit')->name('vgroups_form');
    Route::get('/vgroups/form/{id}', 'VgroupController@edit');
    Route::post('/vgroups/form', 'VgroupController@store');
    Route::post('/vgroups/delete', 'VgroupController@destroy')->name('vgroups_destroy');
    Route::post('/vgroups/exchange', 'VgroupController@exchange')->name('vgroups_exchange');

    Route::get('/variations', 'VariationController@index')->name('variations');
    Route::post('/variations/json', 'VariationController@json')->name('variations_json');
    Route::get('/variations/form', 'VariationController@edit')->name('variations_form');
    Route::get('/variations/form/{id}', 'VariationController@edit');
    Route::post('/variations/form', 'VariationController@store');
    Route::post('/variations/delete', 'VariationController@destroy')->name('variations_destroy');
    Route::post('/variations/exchange', 'VariationController@exchange')->name('variations_exchange');

    Route::get('/addons', 'AddonController@index')->name('addons');
    Route::post('/addons/json', 'AddonController@json')->name('addons_json');
    Route::get('/addons/form', 'AddonController@edit')->name('addons_form');
    Route::get('/addons/form/{id}', 'AddonController@edit');
    Route::post('/addons/form', 'AddonController@store');
    Route::post('/addons/delete', 'AddonController@destroy')->name('addons_destroy');
    Route::post('/addons/exchange', 'AddonController@exchange')->name('addons_exchange');

    Route::get('/timetables', 'TimetableController@index')->name('timetables');
    Route::post('/timetables/json', 'TimetableController@json')->name('timetables_json');
    Route::get('/timetables/form', 'TimetableController@edit')->name('timetables_form');
    Route::get('/timetables/form/{id}', 'TimetableController@edit');
    Route::post('/timetables/form', 'TimetableController@store');
    Route::post('/timetables/delete', 'TimetableController@destroy')->name('timetables_destroy');
    Route::post('/timetables/exchange', 'TimetableController@exchange')->name('timetables_exchange');

    Route::get('/addresses', 'AddressController@index')->name('addresses');
    Route::post('/addresses/json', 'AddressController@json')->name('addresses_json');
    Route::get('/addresses/form', 'AddressController@edit')->name('addresses_form');
    Route::get('/addresses/form/{id}', 'AddressController@edit');
    Route::post('/addresses/form', 'AddressController@store');
    Route::post('/addresses/delete', 'AddressController@destroy')->name('addresses_destroy');
    Route::post('/addresses/exchange', 'AddressController@exchange')->name('addresses_exchange');

    Route::get('/products', 'ProductController@index')->name('products');
    Route::post('/products/json', 'ProductController@json')->name('products_json');
    Route::get('/products/form', 'ProductController@edit')->name('products_form');
    Route::get('/products/form/{id}', 'ProductController@edit');
    Route::post('/products/form', 'ProductController@store');
    Route::post('/products/delete', 'ProductController@destroy')->name('products_destroy');
    Route::post('/products/exchange', 'ProductController@exchange')->name('products_exchange');



}); /* Routes per la pagina di gestione per i vari form, aggiungere o eliminare gli elementi */

Route::get('/', 'SiteController@index')->name('home'); /* Route per la visualizzazione della home page */
Route::get('/menu', 'SiteController@menu')->name('menu'); /* route per i menu */
Route::get('/termini-e-condizioni', 'SiteController@termini')->name('termini-e-condizioni'); /* route dei termini e le condizioni */
Route::get('/menu/{slug}', 'SiteController@menu'); /* route per ??? */

Route::get('/cart_variations_form/{id}', 'SiteController@cart_variations_form');

Route::post('/products/json', 'ProductController@json')->name('products_json');




Route::get('/migrate', function () {
    Artisan::call("migrate");
    echo "done";
});
